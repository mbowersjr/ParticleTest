using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace ParticleTest.Util
{
    /// <summary>
    /// Handles efficient rendering of lines, points, and triangles, in a similar way to <see cref="SpriteBatch"/>.
    /// </summary>
    public class PrimitiveBatch : IDisposable
    {
        private const int DefaultBufferSize = 500;

        private readonly VertexPositionColor[] _vertices = new VertexPositionColor[DefaultBufferSize];
        private int _positionInBuffer;
        private readonly BasicEffect _basicEffect;
        private readonly GraphicsDevice _graphics;
        private PrimitiveType _primitiveType;
        private int _verticesPerPrimitive;
        private bool _hasBegun;

        /// <summary>
        /// Creates a new <see cref="PrimitiveBatch"/> object.
        /// </summary>
        /// <param name="graphicsDevice">The <see cref="GraphicsDevice"/> used by this <see cref="PrimitiveBatch"/></param>
        public PrimitiveBatch(GraphicsDevice graphicsDevice)
        {
            if (graphicsDevice == null)
            {
                throw new ArgumentNullException("graphicsDevice");
            }

            _graphics = graphicsDevice;

            _basicEffect = new BasicEffect(graphicsDevice)
            {
                VertexColorEnabled = true,
                Projection = Matrix.CreateOrthographicOffCenter(0, graphicsDevice.Viewport.Width, graphicsDevice.Viewport.Height, 0, 0, 1)
            };
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        private bool _isDisposed;
        protected virtual void Dispose(bool disposing)
        {
            if (disposing && !_isDisposed)
            {
                if (_basicEffect != null)
                    _basicEffect.Dispose();

                _isDisposed = true;
            }
        }

        /// <summary>
        /// Begins a primitive batch operation using the specified primitive type.
        /// </summary>
        /// <param name="primitiveType">Primitive type.</param>
        public void Begin(PrimitiveType primitiveType)
        {
            if (_hasBegun)
            {
                throw new InvalidOperationException
                    ("End must be called before Begin can be called again.");
            }

            // These three types reuse vertices, so we can't flush properly without more
            // complex logic. Since that's a bit too complicated for this sample, we'll simply disallow them.
            if (primitiveType == PrimitiveType.LineStrip || primitiveType == PrimitiveType.TriangleStrip)
            {
                throw new NotSupportedException("The specified primitiveType is not supported by PrimitiveBatch.");
            }

            _primitiveType = primitiveType;
            _verticesPerPrimitive = GetVertexCountForPrimitiveType(primitiveType);

            _basicEffect.CurrentTechnique.Passes[0].Apply();
            
            _hasBegun = true;
        }

        /// <summary>
        /// Adds another vertex to be rendered.
        /// </summary>
        /// <param name="vertex">Vertex to add.</param>
        /// <param name="color">Color of <paramref name="vertex"/>.</param>
        /// <remarks>
        /// <para>This function can only be called once <see cref="Begin"/> has been called.</para>
        /// <para>If there is not enough room in the vertices buffer, <see cref="Flush"/> is called automatically.</para>
        /// </remarks>
        public void AddVertex(Vector2 vertex, Color color)
        {
            if (!_hasBegun)
            {
                throw new InvalidOperationException
                    ("Begin must be called before AddVertex can be called.");
            }

            // are we starting a new primitive? if so, and there will not be enough room
            // for a whole primitive, flush.
            bool newPrimitive = ((_positionInBuffer % _verticesPerPrimitive) == 0);

            if (newPrimitive &&
                (_positionInBuffer + _verticesPerPrimitive) >= _vertices.Length)
            {
                Flush();
            }

            // once we know there's enough room, set the vertex in the buffer,
            // and increase position.
            _vertices[_positionInBuffer].Position = new Vector3(vertex, 0);
            _vertices[_positionInBuffer].Color = color;

            _positionInBuffer++;
        }

        /// <summary>
        /// Flushes the <see cref="PrimitiveBatch"/> once all primitives have been drawn using <see cref="AddVertex"/>.
        /// </summary>
        /// <remarks>
        /// <see cref="Begin"/> must be called before <see cref="End"/> can be called.
        /// </remarks>
        /// <exception cref="InvalidOperationException"><see cref="Begin"/> must be called before <see cref="End"/> can be called.</exception>
        public void End()
        {
            if (!_hasBegun)
            {
                throw new InvalidOperationException("Begin must be called before End can be called.");
            }

            // Draw whatever the user wanted us to draw
            Flush();

            _hasBegun = false;
        }

        /// <summary>
        /// Draws primitives to the <see cref="GraphicsDevice"/> and resets the vertex buffer.
        /// </summary>
        /// <exception cref="InvalidOperationException"><see cref="Begin"/> must be called before <see cref="Flush"/> can be called.</exception>
        private void Flush()
        {
            if (!_hasBegun)
            {
                throw new InvalidOperationException("Begin must be called before Flush can be called.");
            }

            if (_positionInBuffer == 0)
                return;

            var primitiveCount = _positionInBuffer / _verticesPerPrimitive;
            _graphics.DrawUserPrimitives(_primitiveType, _vertices, 0, primitiveCount);

            _positionInBuffer = 0;
        }

        // NumVertsPerPrimitive is a boring helper function that tells how many vertices
        // it will take to draw each kind of primitive.
        /// <summary>
        /// Returns the number of vertices required for the specified <see cref="PrimitiveType"/>.
        /// </summary>
        /// <param name="primitiveType">The <see cref="PrimitiveType"/>.</param>
        /// <returns>The number of vertices required for <paramref name="primitiveType"/>.</returns>
        private static int GetVertexCountForPrimitiveType(PrimitiveType primitiveType)
        {
            int vertexCount;

            switch (primitiveType)
            {
                case PrimitiveType.LineList:
                    vertexCount = 2;
                    break;
                case PrimitiveType.TriangleList:
                    vertexCount = 3;
                    break;
                default:
                    throw new ArgumentException("Invalid primitive type.", "primitiveType");
            }
            
            return vertexCount;
        }
    }
}
