﻿using System;
using Microsoft.Xna.Framework;

namespace ParticleTest.Util
{
    public static class VariationHelper
    {
        private static readonly Random Rand = new Random();

        public static float GetVariation(float average, float variation)
        {
            //return Rand.NextFloat() * average + Rand.NextFloat() * variation;
            return (float)(average + ((Rand.NextDouble() - 0.5) * variation));
        }

        public static double GetVariation(double average, double variation)
        {
            return average + ((Rand.NextDouble() - 0.5) * variation);
        }

        public static int GetVariation(int average, int variation)
        {
            return (int)(average + ((Rand.NextDouble() - 0.5) * variation));
        }

        /// <summary>
        /// Returns a <see cref="Microsoft.Xna.Framework.Vector2"/> with a random variation in rotation.
        /// </summary>
        /// <param name="average">The average vector.</param>
        /// <param name="variation">Rotation variation range in radians.</param>
        /// <returns>
        /// A <see cref="F:Microsoft.Xna.Framework.Vector2"/> with a variation in rotation of <paramref name="variation"/> radians.
        /// </returns>
        public static Vector2 GetVariation(Vector2 average, float variation)
        {
            var rotation = GetVariation(average.ToRadians(), variation);

            return rotation.ToVector2() * average.Length();
        }
    }
}
