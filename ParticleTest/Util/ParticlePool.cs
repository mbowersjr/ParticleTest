﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using ParticleTest.Entities;

namespace ParticleTest.Util
{
    public class ParticlePool
    {
        public int Count { get; private set; }
        public int Length { get { return _particles.Length; } }

        private int _startIndex;
        public int StartIndex
        {
            get { return _startIndex; }
            set { _startIndex = value % _particles.Length; }
        }

        public Particle this[int i]
        {
            get { return _particles[(_startIndex + i) % _particles.Length]; }
            set { _particles[(_startIndex + i) % _particles.Length] = value; }
        }

        private readonly Particle[] _particles;
        
        public ParticlePool(int capacity)
        {
            _particles = new Particle[capacity];
        }

        public void Update(int index, GameTime gameTime)
        {
            _particles[index].Update(gameTime);
        }

        public void Add(Particle particle)
        {
            if (Count == Length)
            {
                _particles[0] = particle;
                StartIndex++;
            }
            else
            {
                _particles[Count] = particle;
                Count++;
            }
        }
        
        public void Reorder()
        {
            var removalCount = 0;

            for (var i = 0; i < Count; i++)
            {
                if (!_particles[i].IsActive)
                {
                    Swap(Count - 1 - removalCount++, i);
                }
            }
            
            Count -= removalCount;
        }

        public void Clear() 
        {
            for (var i = 0; i < Count; i++) 
            {
                _particles[i].IsActive = false;
            }

            Count = 0;
        }

        private void Swap(int index1, int index2)
        {
            var temp = _particles[index1];
            _particles[index1] = _particles[index2];
            _particles[index2] = temp;
        }
    }
}
