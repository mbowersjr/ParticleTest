﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using ParticleTest.Managers;

namespace ParticleTest.Util
{
    public enum ZeroDirection
    {
        Up,
        Right
    }

    public static class ExtensionMethods
    {
        public static bool ZeroDegreesIsUp = true;

        public static ModifierKeysState GetModifierKeysState(this KeyboardState keyboardState)
        {
            return new ModifierKeysState(keyboardState);
        }

        public static T Clamp<T>(this T value, T min, T max) where T : IComparable<T>
        {
            value = value.CompareTo(max) > 0 ? max : value;
            value = value.CompareTo(min) < 0 ? min : value;

            return value;
        }

        public static Vector2 GetPosition(this MouseState mouseState)
        {
            return new Vector2(mouseState.X, mouseState.Y);
        }
        
        public static Vector2 ToVector2(this Point point)
        {
            return new Vector2(point.X, point.Y);
        }

        public static Point ToPoint(this Vector2 vector)
        {
            return new Point((int)vector.X, (int)vector.Y);
        }

        public static Vector2 ToVector2(this float angle)
        {
            return ZeroDegreesIsUp ? 
                new Vector2((float)Math.Sin(angle), -(float)Math.Cos(angle)) : 
                new Vector2((float)Math.Cos(angle), (float)Math.Sin(angle));
        }

        public static float ToRadians(this Vector2 vector)
        {
            return ZeroDegreesIsUp ? 
                (float)Math.Atan2(vector.X, -vector.Y) : 
                (float)Math.Atan2(vector.Y, vector.X);
        }

        public static float RotationToTarget(this Vector2 origin, Vector2 target)
        {
            var direction = target - origin;
            return (float)Math.Atan2(direction.Y, direction.X);
        }

        public static void GetRotationToTarget(ref Vector2 origin, ref Vector2 target, out float radians)
        {
            var direction = target - origin;
            radians = (float)Math.Atan2(direction.Y, direction.X);
        }

        public static Vector2 Rotate(this Vector2 vector, float angle)
        {
            return Vector2.Transform(vector, Matrix.CreateRotationX(angle));
        }
        
        public static void DrawLine(this SpriteBatch spriteBatch, Vector2 start, Vector2 end, int width, Color color)
        {
            var length = (end - start).Length();
            var midpoint = new Vector2(start.X + (end.X - start.X) / 2, start.Y + (end.Y - start.Y) / 2);
            var rotation = start.RotationToTarget(end);

            spriteBatch.Draw(
                texture: ResourceManager.PixelTexture.Texture, 
                position: midpoint,
                sourceRectangle: null, 
                color: color,
                rotation: rotation, 
                origin: ResourceManager.PixelTexture.Origin,
                scale: new Vector2(length, width),
                effects: SpriteEffects.None, 
                layerDepth: 0
            );
        }

        public static float NextFloat(this Random rand, float minValue, float maxValue)
        {
            return (float)(rand.NextDouble() * (maxValue - minValue) + minValue);
        }

        public static float NextFloat(this Random rand)
        {
            return (float)rand.NextDouble();
        }
    }
}
