﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using ParticleTest.Managers;

namespace ParticleTest.Util
{
    public class FrameRateCounter : DrawableGameComponent
    {
        public int FrameRate { get; private set; }

        private int _frameCounter;
        private long _elapsedTicks;
        
        public FrameRateCounter(Game game)
            : base(game)
        {
        }
        
        public override void Update(GameTime gameTime)
        {
            _elapsedTicks += gameTime.ElapsedGameTime.Ticks;

            if (_elapsedTicks > TimeSpan.TicksPerSecond)
            {
                _elapsedTicks -= TimeSpan.TicksPerSecond;
                FrameRate = _frameCounter;
                _frameCounter = 0;
            }
        }

        public override void Draw(GameTime gameTime)
        {
            _frameCounter++;
        }
    }
}
