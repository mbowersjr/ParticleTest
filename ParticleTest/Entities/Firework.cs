﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using ParticleTest.Managers;
using ParticleTest.Util;

namespace ParticleTest.Entities
{
    public class Firework : IEntity
    {
        public Color ParticleColor { get; private set; }

        public double Age { get; set; }
        public double Fuse { get; set; }
        public int ParticleCount { get; set; }
        public bool IsActive { get; set; }
        public float Scale { get; set; }
        public Color Color { get { return ParticleColor; } }
        public Vector2 Acceleration { get; set; }

        private Vector2 _position;
        public Vector2 Position
        {
            get { return _position; }
            set { _position = value; }
        }

        private Vector2 _velocity;
        public Vector2 Velocity
        {
            get { return _velocity; }
            set { _velocity = value; }
        }

        private static readonly float ParticleVelocity = 200f;
        private static readonly float ParticleVelocityVariation = 50f;
        
        private static readonly Random Rand = new Random();
        private readonly TimeSpan _particleLifetime = TimeSpan.FromSeconds(2);
        
        public Firework(Vector2 position, Vector2 velocity, TimeSpan fuse, int particleCount, Color particleColor)
        {
            Position = position;
            Velocity = velocity;
            Scale = 0.5f;
            Fuse = fuse.TotalMilliseconds;
            ParticleColor = particleColor;
            ParticleCount = particleCount;

            IsActive = true;
        }

        public void Update(GameTime gameTime)
        {
            Fuse -= gameTime.ElapsedGameTime.TotalMilliseconds;

            if (Fuse <= 0)
            {
                IsActive = false;
                Explode();

                return;
            }

            var elapsedTotalSeconds = (float)gameTime.ElapsedGameTime.TotalSeconds;

            Acceleration = ParticleManager.Gravity;
            var oldVelocity = Velocity;
            Velocity += Acceleration * elapsedTotalSeconds;
            Position += (oldVelocity + Velocity) * 0.5f * elapsedTotalSeconds;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(
                texture: ResourceManager.CircleTexture.Texture,
                position: Position,
                sourceRectangle: null,
                color: Color,
                rotation: 0,
                origin: ResourceManager.CircleTexture.Origin,
                scale: Scale,
                effects: SpriteEffects.None,
                layerDepth: 0
            );
        }

        protected void Explode()
        {
            for (var i = 0; i < ParticleCount; i++)
            {
                var velocity = (Rand.NextFloat() * MathHelper.TwoPi).ToVector2() * Rand.NextFloat() * ParticleVelocity;
                var particle = new Particle(Position, velocity, ParticleColor, _particleLifetime);

                ParticleTestGame.ParticleManager.AddParticle(particle);
            }
        }
    }
}
