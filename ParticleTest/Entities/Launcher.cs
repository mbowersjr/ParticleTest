﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using ParticleTest.Managers;
using ParticleTest.Util;

namespace ParticleTest.Entities
{
    public class Launcher : IAdjustable
    {
        public bool IsActive { get; set; }
        public Vector2 Position { get; set; }
        public float Scale { get; private set; }
        public Color Color { get; private set; }

        public bool IsAdjusting { get; set; }
        public bool IsSelected { get; set; }
        public bool IsBeingMoved { get; set; }
        
        public Color FireworkssColor { get; set; }
        public double FireRate { get; set; }

        public float Force { get; private set; }
        public float Angle { get; private set; }

        private Vector2 _target;
        public Vector2 Target
        {
            get { return _target; }
            set { Velocity = value - Position; }
        }

        private Vector2 _velocity;
        public Vector2 Velocity
        {
            get { return _velocity; }
            set
            {
                _velocity = value;
                Angle = _velocity.ToRadians();
                Force = _velocity.LengthSquared();
                _target = Position + _velocity;
            }
        }

        private float _variation;
        public float Variation
        {
            get { return _variation; }
            set { _variation = Math.Max(0, value); }
        }

        private readonly static double FireRateAdjustmentAmount = TimeSpan.FromSeconds(1).TotalMilliseconds;
        public static readonly float VariationAdjustmentAmount = MathHelper.ToRadians(1);

        private int _shiftKeyModifier;
        private double _lastFire;

        private static readonly Random Rand = new Random();

        public string ControlsDisplay => @"
[Up][Down]  Fire rate
    [C]     Fireworks color
    [Del]   Delete launcher";

        public string SelectionDisplay => $"Rate:     {FireRate}\nVelocity: {Velocity.Length()}\nColor:    {FireworkssColor}";

        public Launcher(Vector2 position, Vector2 velocity, TimeSpan fireRate)
        {
            Position = position;
            Velocity = velocity;
            Scale = 0.75f;
            Color = Color.White;
            FireRate = fireRate.TotalMilliseconds;
            FireworkssColor = ParticleManager.YellowParticleColor;
            IsActive = true;
        }

        public void HandleInput(InputManager inputManager, GameTime gameTime)
        {
            if (inputManager.WasKeyPressed(Keys.C))
            {
                if (FireworkssColor == ParticleManager.YellowParticleColor)
                    FireworkssColor = ParticleManager.GreenParticleColor;
                else if (FireworkssColor == ParticleManager.GreenParticleColor)
                    FireworkssColor = ParticleManager.BlueParticleColor;
                else if (FireworkssColor == ParticleManager.BlueParticleColor)
                    FireworkssColor = ParticleManager.RedParticleColor;
                else
                    FireworkssColor = ParticleManager.YellowParticleColor;
            }

            if (!inputManager.ShouldHandleRepeatingInput(gameTime))
                return;

            _shiftKeyModifier = inputManager.IsShiftKeyDown() ? 2 : 1;

            if (inputManager.IsKeyDown(Keys.Up))
            {
                // Up
                FireRate += (FireRateAdjustmentAmount * _shiftKeyModifier);
            }
            else if (inputManager.IsKeyDown(Keys.Down))
            {
                // Down
                FireRate -= (FireRateAdjustmentAmount * _shiftKeyModifier);
            }

            if (inputManager.IsKeyDown(Keys.Left))
            {
                if (inputManager.IsControlKeyDown())
                {
                    // Ctrl+Left
                }
                else
                {
                    // Left
                    Variation -= VariationAdjustmentAmount * _shiftKeyModifier;
                }
            }
            else if (inputManager.IsKeyDown(Keys.Right))
            {
                if (inputManager.IsControlKeyDown())
                {
                    // Ctrl+Right
                }
                else
                {
                    // Right
                    Variation += VariationAdjustmentAmount * _shiftKeyModifier;
                }
            }
        }

        private static int FireworksParticleCount = 2000;

        public void Update(GameTime gameTime)
        {
            if (gameTime.TotalGameTime.TotalMilliseconds - _lastFire >= FireRate)
            {
                _lastFire = gameTime.TotalGameTime.TotalMilliseconds;

                var firework = new Firework(
                    Position, 
                    VariationHelper.GetVariation(Velocity, Variation), 
                    TimeSpan.FromSeconds(3), 
                    FireworksParticleCount, 
                    FireworkssColor);

                ParticleTestGame.ParticleManager.AddEntity(firework);
            }
        }

        public void HandleInputForAdjustment(InputManager inputManager)
        {
            if (IsBeingMoved)
            {
                var position = Position;
                position.X = inputManager.MousePosition.X;
                Position = position;

                Target = Position + Velocity;
            }
            else if (IsAdjusting)
            {
                Target = inputManager.MousePosition;
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            if (IsSelected)
            {
                DrawTargetIndicator(spriteBatch);

                // Outline when selected
                spriteBatch.Draw(
                    texture: ResourceManager.CircleTexture.Texture,
                    position: Position,
                    sourceRectangle: null,
                    color: Color.Black,
                    rotation: 0,
                    origin: ResourceManager.CircleTexture.Origin,
                    scale: Scale * 1.2f,
                    effects: SpriteEffects.None,
                    layerDepth: 0
                );
            }

            spriteBatch.Draw(
                texture: ResourceManager.CircleTexture.Texture,
                position: Position,
                sourceRectangle: null,
                color: Color,
                rotation: 0,
                origin: ResourceManager.CircleTexture.Origin,
                scale: Scale,
                effects: SpriteEffects.None,
                layerDepth: 0
            );
        }

        public void DrawTargetIndicator(SpriteBatch spriteBatch)
        {
            if (IsAdjusting)
            {
                spriteBatch.DrawLine(
                    start: Position,
                    end: Target,
                    width: TargetIndicatorProperties.LineThickness,
                    color: TargetIndicatorProperties.LineColor
                );
            }

            spriteBatch.Draw(
                texture: ResourceManager.CircleTexture.Texture,
                position: Target,
                sourceRectangle: null,
                color: TargetIndicatorProperties.HandleColor,
                rotation: 0,
                origin: ResourceManager.CircleTexture.Origin,
                scale: TargetIndicatorProperties.HandleScale,
                effects: SpriteEffects.None,
                layerDepth: 0
            );
        }
    }
}
