﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using ParticleTest.Managers;
using ParticleTest.Util;

namespace ParticleTest.Entities
{
    public class Forcefield : IAdjustable, IModifier
    {
        public bool IsActive { get; set; }
        public Vector2 Position { get; set; }
        public float Scale { get { return 0.75f; } }

        public Color Color { get; protected set; }

        private int _forceAbsSquared;
        private int _force;
        public int Force
        {
            get { return _force; }
            set
            {
                _force = value;
                _forceAbsSquared = _force * Math.Abs(_force);
                Target = new Vector2(Position.X, Position.Y + _force);
                Color = _force < 0 ? Color.OrangeRed : Color.LimeGreen;
            }
        }

        public bool IsSelected { get; set; }

        public string SelectionDisplay => $"Force: {Force}";

        public string ControlsDisplay => string.Empty;

        public bool IsAdjusting { get; set; }
        public Vector2 Target { get; set; }
        public bool IsBeingMoved { get; set; }
        

        public Forcefield(Vector2 position, int force)
        {
            Color = Color.LimeGreen;
            Position = position;
            Force = force;
            IsActive = true;
        }
        
        public void HandleInputForAdjustment(InputManager inputManager)
        {
            if (IsBeingMoved)
            {
                Position = inputManager.MousePosition;
                Target = new Vector2(Position.X, Position.Y + Force);
            }
            else if (IsAdjusting)
            {
                Target = inputManager.MousePosition;
                Force = (int)(inputManager.MousePosition - Position).Y;
            }
        }

        public void DrawTargetIndicator(SpriteBatch spriteBatch)
        {
            if (IsAdjusting)
            {
                spriteBatch.DrawLine(
                    start: Position, 
                    end: Target, 
                    width: TargetIndicatorProperties.LineThickness, 
                    color: TargetIndicatorProperties.LineColor
                );
            }

            spriteBatch.Draw(
                texture: ResourceManager.CircleTexture.Texture, 
                position: Target, 
                sourceRectangle: null, 
                color: TargetIndicatorProperties.HandleColor, 
                rotation: 0, 
                origin: ResourceManager.CircleTexture.Origin, 
                scale: TargetIndicatorProperties.HandleScale, 
                effects: SpriteEffects.None, 
                layerDepth: 0
            );
        }

        public void Update(GameTime gameTime)
        {
            
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            if (IsSelected)
            {
                DrawTargetIndicator(spriteBatch);

                // Outline when selected
                spriteBatch.Draw(
                    texture: ResourceManager.CircleTexture.Texture,
                    position: Position,
                    sourceRectangle: null,
                    color: Color.Black,
                    rotation: 0,
                    origin: ResourceManager.CircleTexture.Origin,
                    scale: Scale * 1.5f,
                    effects: SpriteEffects.None,
                    layerDepth: 0
                );
            }
            
            spriteBatch.Draw(
                texture: ResourceManager.CircleTexture.Texture, 
                position: Position, 
                sourceRectangle: null, 
                color: Color, 
                rotation: 0, 
                origin: ResourceManager.CircleTexture.Origin, 
                scale: Scale, 
                effects: SpriteEffects.None, 
                layerDepth: 0
            );
        }

        public void HandleInput(InputManager inputManager, GameTime gameTime)
        {
            if (!inputManager.ShouldHandleRepeatingInput(gameTime))
                return;

            if (inputManager.IsKeyDown(Keys.Up))
            {
                Force += 1;
            }
            else if (inputManager.IsKeyDown(Keys.Down))
            {
                Force -= 1;
            }
        }

        
        public void ModifyParticle(ref Particle particle)
        {
            var distance = Position - particle.Position;
            var effect = ParticleManager.GravitationalForce * (_forceAbsSquared / distance.LengthSquared());
            
            particle.Acceleration += Vector2.Normalize(distance) * effect;
        }
    }
}
