﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using ParticleTest.Managers;

namespace ParticleTest.Entities
{
    public struct Particle
    {
        public const float ParticleScale = 2.0f;

        public bool IsActive;
        public Vector2 Position;
        public Vector2 Velocity;
        public Vector2 Acceleration;
        public float Scale;
        public Color Color;
        public readonly double Lifetime;
        public double Age;

        //private Vector2 _currentPosition;
        //private Vector2 _previousPosition;
        //private readonly ParticleManager _particleManager;

        public Particle(Vector2 position, Vector2 velocity, Color color, TimeSpan lifetime)
        {
            Position = position;
            //_previousPosition = position;
            //_currentPosition = position;
            
            Velocity = velocity;
            Acceleration = ParticleManager.Gravity;
            Scale = ParticleScale;
            Lifetime = lifetime.TotalMilliseconds;
            Age = 0;
            Color = color;

            IsActive = true;
        }

        public void Update(GameTime gameTime)
        {
            Age += gameTime.ElapsedGameTime.TotalMilliseconds;
            
            if (Age > Lifetime)
            {
                IsActive = false;
                return;
            }

            var elapsedTotalSeconds = (float)gameTime.ElapsedGameTime.TotalSeconds;

            Acceleration = ParticleManager.Gravity;
            foreach (var modifier in ParticleTestGame.ParticleManager.Modifiers)
            {
                modifier.ModifyParticle(ref this);
            }

            var oldVelocity = Velocity;
            Velocity += Acceleration * elapsedTotalSeconds;
            Position += (oldVelocity + Velocity) * 0.5f * elapsedTotalSeconds;

            if (Position.Y > ParticleTestGame.ClientBounds.Height)
            {
                if (!ParticleManager.WindowBoundriesEnabled)
                {
                    IsActive = false;
                    return;
                }
                
                Position.Y = ParticleTestGame.ClientBounds.Height;
                Velocity.Y *= ParticleManager.BounceReflectionModifier;
            }

            if (Position.X < 0)
            {
                Position.X = 0;
                Velocity.X *= ParticleManager.BounceReflectionModifier;
            }
            else if (Position.X > ParticleTestGame.ClientBounds.Width)
            {
                Position.X = ParticleTestGame.ClientBounds.Width;
                Velocity.X *= ParticleManager.BounceReflectionModifier;
            }
        }
        
        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(
                texture: ResourceManager.PixelTexture.Texture,
                position: Position,
                sourceRectangle: null,
                color: (ParticleManager.ParticleFadingEnabled) ? Color * (float)(1.0 - (Age / Lifetime)) : Color,
                rotation: 0,
                origin: ResourceManager.PixelTexture.Origin,
                scale: Scale,
                effects: SpriteEffects.None,
                layerDepth: 0
            );
        }
    }
}
