﻿using Microsoft.Xna.Framework;
using ParticleTest.Managers;

namespace ParticleTest.Entities
{
    public interface ISelectable
    {
        bool IsSelected { get; set; }
        string SelectionDisplay { get; }

        void HandleInput(InputManager inputManager, GameTime gameTime);
    }
}
