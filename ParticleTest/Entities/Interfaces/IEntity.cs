﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace ParticleTest.Entities
{
    public interface IEntity
    {
        bool IsActive { get; set; }
        Vector2 Position { get; set; }
        float Scale { get; }
        Color Color { get; }
        
        void Update(GameTime gameTime);
        void Draw(SpriteBatch spriteBatch);
    }
}
