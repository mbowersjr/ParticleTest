﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace ParticleTest.Entities
{
    public interface IModifier
    {
        void ModifyParticle(ref Particle particle);
    }
}
