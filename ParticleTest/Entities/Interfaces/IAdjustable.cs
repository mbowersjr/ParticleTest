﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using ParticleTest.Managers;

namespace ParticleTest.Entities
{
    public interface IAdjustable : IEntity, ISelectable
    {
        bool IsAdjusting { get; set; }
        bool IsBeingMoved { get; set; }
        Vector2 Target { get; set; }
        
        string ControlsDisplay { get; }

        void HandleInputForAdjustment(InputManager inputManager);
        void DrawTargetIndicator(SpriteBatch spriteBatch);
    }
}
