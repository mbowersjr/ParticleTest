﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using ParticleTest.Managers;
using ParticleTest.Util;

namespace ParticleTest.Entities
{
    public class Emitter : IAdjustable
    {
        public static readonly int SpawnRateAdjustmentAmount = 5;
        public static readonly float VariationAdjustmentAmount = MathHelper.ToRadians(1);
        
        private Vector2 _position;
        public Vector2 Position
        {
            get { return _position; }
            set
            {
                _position = value;
                _target = _position + _velocity;
            }
        }

        public float Scale => 0.75f;
        public Color Color => Color.Red;

        public bool IsActive { get; set; }
        public bool IsSelected { get; set; }
        public bool IsBeingMoved { get; set; }
        public bool IsAdjusting { get; set; }

        public string SelectionDisplay => $@"
Rate:     {ParticlesPerSecond}
Angle:    {Angle}
Velocity: {Force}
Variance: +- {MathHelper.ToDegrees(Variation / 2)} deg.
Color:    {ParticleColor}";

        public string ControlsDisplay => @"
  [Up][Down]  Particles per second
[Left][Right] Particle velocity variation
      [Shift] Increase change rate
      [C]     Particle color
      [Del]   Delete emitter";

        private Vector2 _target;
        public Vector2 Target
        {
            get { return _target; }
            set { Velocity = value - Position; }
        }

        public Color ParticleColor { get; set; }

        private TimeSpan _particleLifetime;
        public TimeSpan ParticleLifetime
        {
            get { return _particleLifetime; }
            set {  _particleLifetime = value < TimeSpan.Zero ? TimeSpan.Zero : value; }
        }

        private int _countPerSpawn;
        private int _particlesPerSecond;
        public int ParticlesPerSecond
        {
            get { return _particlesPerSecond; }
            set
            {
                _particlesPerSecond = Math.Max(value, 1);
                _countPerSpawn = (int)(_particlesPerSecond / TimeSpan.FromSeconds(1.0 / 60).TotalMilliseconds);
            }
        }

        private float _variation;
        public float Variation
        {
            get { return _variation; }
            set { _variation = Math.Max(0, value); }
        }

        public float Force { get; private set; }
        public float Angle { get; private set; }

        private Vector2 _velocity;
        public Vector2 Velocity
        {
            get { return _velocity; }
            set
            {
                _velocity = value;
                Angle = _velocity.ToRadians();
                Force = _velocity.LengthSquared();
                _target = Position + _velocity;
            }
        }
        
        private readonly double _spawnRate = TimeSpan.FromSeconds(1.0 / 60).TotalMilliseconds;
        private double _lastSpawn;
        
        public Emitter(Vector2 position, Vector2 velocity, int rate)
        {
            Position = position;
            Velocity = velocity;
            ParticleColor = ParticleManager.YellowParticleColor;
            Variation = MathHelper.ToRadians(25);
            ParticleLifetime = TimeSpan.FromSeconds(10);
            ParticlesPerSecond = rate;
            IsActive = true;
        }

        public void Update(GameTime gameTime)
        {
            if (gameTime.TotalGameTime.TotalMilliseconds - _lastSpawn < _spawnRate)
                return;

            _lastSpawn = gameTime.TotalGameTime.TotalMilliseconds;

            for (var i = 0; i < _countPerSpawn; i++)
            {
                var particle = new Particle(Position, VariationHelper.GetVariation(Velocity, Variation), ParticleColor, ParticleLifetime);
                ParticleTestGame.ParticleManager.AddParticle(particle);
            }
        }
        
        private int _shiftKeyModifier;
        
        public void HandleInput(InputManager inputManager, GameTime gameTime)
        {
            if (inputManager.WasKeyPressed(Keys.C))
            {
                if (ParticleColor == ParticleManager.YellowParticleColor)
                    ParticleColor = ParticleManager.GreenParticleColor;
                else if (ParticleColor == ParticleManager.GreenParticleColor)
                    ParticleColor = ParticleManager.BlueParticleColor;
                else if (ParticleColor == ParticleManager.BlueParticleColor)
                    ParticleColor = ParticleManager.RedParticleColor;
                else
                    ParticleColor = ParticleManager.YellowParticleColor;
            }

            if (!inputManager.ShouldHandleRepeatingInput(gameTime))
                return;

            _shiftKeyModifier = inputManager.IsShiftKeyDown() ? 2 : 1;

            if (inputManager.IsKeyDown(Keys.Up))
            {
                // Up
                ParticlesPerSecond += (SpawnRateAdjustmentAmount * _shiftKeyModifier);
            }
            else if (inputManager.IsKeyDown(Keys.Down))
            {
                // Down
                ParticlesPerSecond -= (SpawnRateAdjustmentAmount * _shiftKeyModifier);
            }

            if (inputManager.IsKeyDown(Keys.Left))
            {
                if (inputManager.IsControlKeyDown())
                {
                    // Ctrl+Left
                }
                else
                {
                    // Left
                    Variation -= VariationAdjustmentAmount * _shiftKeyModifier;
                }
            }
            else if (inputManager.IsKeyDown(Keys.Right))
            {
                if (inputManager.IsControlKeyDown())
                {
                    // Ctrl+Right
                }
                else
                {
                    // Right
                    Variation += VariationAdjustmentAmount * _shiftKeyModifier;
                }
            }
        }
        
        public void HandleInputForAdjustment(InputManager inputManager)
        {
            if (IsBeingMoved)
            {
                Position = inputManager.MousePosition;
                Target = Position + Velocity;
            }
            else if (IsAdjusting)
            {
                Target = inputManager.MousePosition;
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            if (IsSelected)
            {
                DrawTargetIndicator(spriteBatch);

                // Outline when selected
                spriteBatch.Draw(
                    texture: ResourceManager.CircleTexture.Texture, 
                    position: Position, 
                    sourceRectangle: null,
                    color: Color.Black, 
                    rotation: 0,
                    origin: ResourceManager.CircleTexture.Origin, 
                    scale: Scale * 1.2f, 
                    effects: SpriteEffects.None, 
                    layerDepth: 0
                );
            }
            
            spriteBatch.Draw(
                texture: ResourceManager.CircleTexture.Texture,
                position: Position, 
                sourceRectangle: null,
                color: Color, 
                rotation: 0, 
                origin: ResourceManager.CircleTexture.Origin,
                scale: Scale, 
                effects: SpriteEffects.None,
                layerDepth: 0
            );
        }

        public void DrawTargetIndicator(SpriteBatch spriteBatch)
        {
            if (IsAdjusting)
            {
                spriteBatch.DrawLine(
                    start: Position, 
                    end: Target, 
                    width: TargetIndicatorProperties.LineThickness, 
                    color: TargetIndicatorProperties.LineColor
                );
            }
            
            spriteBatch.Draw(
                texture: ResourceManager.CircleTexture.Texture,
                position: Target,
                sourceRectangle: null,
                color: TargetIndicatorProperties.HandleColor,
                rotation: 0,
                origin: ResourceManager.CircleTexture.Origin,
                scale: TargetIndicatorProperties.HandleScale, 
                effects: SpriteEffects.None, 
                layerDepth: 0
            );
        }
    }

    public static class TargetIndicatorProperties
    {
        public static readonly int LineThickness = 2;
        public static readonly float HandleScale = 0.5f;
        public static readonly Color LineColor = Color.White * 0.75f;
        public static readonly Color HandleColor = Color.White * 0.75f;
    }
}
