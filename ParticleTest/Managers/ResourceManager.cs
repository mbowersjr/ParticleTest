﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace ParticleTest.Managers
{
    public static class ResourceManager
    {
        public static TextureResource PixelTexture { get; private set; }
        public static TextureResource CircleTexture { get; private set; }
        
        public static SpriteFont Font { get; private set; }

        public static void Load(Game game)
        {
            var pixelTexture = new Texture2D(game.GraphicsDevice, 1, 1);
            pixelTexture.SetData(new [] { Color.White });
            PixelTexture = new TextureResource(pixelTexture, new Vector2(0.5f));

            CircleTexture = new TextureResource(game.Content.Load<Texture2D>("circle"));
            
            Font = game.Content.Load<SpriteFont>("font");
        }

        public static void Unload()
        {
            PixelTexture.Dispose();
            CircleTexture.Dispose();
        }
    }
}
