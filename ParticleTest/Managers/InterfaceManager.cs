﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace ParticleTest.Managers
{
    public class InterfaceManager : DrawableGameComponent
    {
        public static Point ScreenMargins = new Point(10, 10);

        public static Color InterfaceTextColor = Color.White * 0.85f;
        public static Color BackgroundColor = new Color(40, 40, 40);
        
        private SpriteBatch _spriteBatch;

        public bool ShowDebugDisplay { get; set; }
        public string DebugDisplay { get; private set; } = string.Empty;
        public string SelectionDisplay { get; private set; } = string.Empty;
        public string Instructions { get; private set; } = string.Empty;

        private Vector2 _debugDisplayPosition;
        private Vector2 _selectionDetailsPosition;
        private Vector2 _instructionsPosition;
        private Rectangle _interfaceBounds;

        private const int MaxVisibleDebugMessages = 10;
        private static readonly List<string> DebugMessages = new List<string>();
        private static string _debugMessagesDisplay = string.Empty;

        private int _performanceDetailsWidth;
        private int _instructionsHeight;
        private int _selectionDetailsHeight;
        
        private static readonly double InterfaceUpdateRate = TimeSpan.FromSeconds(1.0 / 60).TotalMilliseconds;
        private double _updateElapsed;

        public InterfaceManager(Game game)
            : base(game)
        {
            Visible = false;
        }

        /// <summary>
        /// Initializes the component. Override this method to load any non-graphics resources and query for any required services.
        /// </summary>
        public override void Initialize()
        {
            UpdateInstructions();
            SetPositions(ParticleTestGame.ClientBounds, ScreenMargins);

            base.Initialize();
        }

        /// <summary>
        /// Called when graphics resources need to be loaded. Override this method to load any component-specific graphics ResourceManager.
        /// </summary>
        protected override void LoadContent()
        {
            _spriteBatch = new SpriteBatch(GraphicsDevice);
        }

        public void UpdateInstructions()
        {
            Instructions = $@"
[Q] Quit
[R] Reset
[F] Fading Particles:       {ParticleManager.ParticleFadingEnabled}
[B] Floor Boundry:          {ParticleManager.WindowBoundriesEnabled}
[G] Gravity:                {ParticleManager.GravityEnabled}
[D] Debug Display:          {ShowDebugDisplay}
[Shift+P] Postprocessing:   {ParticleManager.BloomEnabled}
[P] Effects Preset:         {(ParticleManager.BloomEnabled ? ParticleManager.CurrentBloomSettingsPreset.ToString() : "Disabled")}
[S] Subframe Interpolation  {ParticleManager.SubframeInterpolationEnabled}";
        }

        private void UpdateDebugDisplay()
        {
            DebugDisplay = $@"
Particle Count:         {ParticleTestGame.ParticleManager.Particles.Count}
Frames Per Second:      {ParticleTestGame.FrameRateCounter.FrameRate}
Subframe Interpolation: {ParticleManager.SubframeInterpolationEnabled}
Postprocessing:         {ParticleManager.BloomEnabled}

{_debugMessagesDisplay}";
        }

        public override void Update(GameTime gameTime)
        {
            _updateElapsed += gameTime.ElapsedGameTime.TotalMilliseconds;

            if (_updateElapsed < InterfaceUpdateRate)
                return;

            _updateElapsed -= InterfaceUpdateRate;

            if (ShowDebugDisplay)
                UpdateDebugDisplay();

            if (ParticleTestGame.ParticleManager != null && ParticleTestGame.ParticleManager.SelectedEntity != null)
            {
                SelectionDisplay = ParticleTestGame.ParticleManager.SelectedEntity.SelectionDisplay;
            }
            else
            {
                SelectionDisplay = null;
            }
        }

        public override void Draw(GameTime gameTime)
        {
            _spriteBatch.Begin();

            _spriteBatch.DrawString(ResourceManager.Font, Instructions, _instructionsPosition, InterfaceTextColor);

            if (ShowDebugDisplay)
            {
                _spriteBatch.DrawString(ResourceManager.Font, DebugDisplay, _debugDisplayPosition, InterfaceTextColor);
            }

            if (!string.IsNullOrEmpty(SelectionDisplay))
            {
                _spriteBatch.DrawString(ResourceManager.Font, SelectionDisplay, _selectionDetailsPosition, InterfaceTextColor);
            }

            _spriteBatch.End();
        }

        private void SetPositions(Rectangle screenBounds, Point padding)
        {
            _interfaceBounds = new Rectangle(padding.X, padding.Y, screenBounds.Width - (padding.X * 2), screenBounds.Height - (padding.Y * 2));

            _performanceDetailsWidth = (int)(_interfaceBounds.Width * 0.3);
            _instructionsHeight = (int)(_interfaceBounds.Height * 0.6);
            _selectionDetailsHeight = (int)(_interfaceBounds.Height * 0.3);

            _debugDisplayPosition = new Vector2(_interfaceBounds.Right - _performanceDetailsWidth, _interfaceBounds.Top);
            _instructionsPosition = new Vector2(_interfaceBounds.Left, _interfaceBounds.Top);
            _selectionDetailsPosition = new Vector2(_interfaceBounds.Left, _interfaceBounds.Bottom - _selectionDetailsHeight);
        }

        public static void DebugWrite(string message)
        {
            DebugMessages.Add(message);

            var messages = DebugMessages.Skip(Math.Max(0, DebugMessages.Count - MaxVisibleDebugMessages)).Take(MaxVisibleDebugMessages);
            _debugMessagesDisplay = string.Join(Environment.NewLine, messages);
        }

        public void DebugClear()
        {
            DebugMessages.Clear();
            UpdateDebugDisplay();
        }
    }
}
