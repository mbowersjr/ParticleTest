﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using ParticleTest.Util;

namespace ParticleTest.Managers
{
    public class MouseDragEventArgs : EventArgs
    {
        public Vector2 StartPosition { get; private set; }
        public Vector2 EndPosition { get; private set; }
        public MouseButtons MouseButton { get; private set; }
        public ModifierKeysState ModifierKeys { get; private set; }

        public MouseDragEventArgs(MouseDragState dragState)
        {
            StartPosition = dragState.StartPosition;
            EndPosition = dragState.EndPosition;
            MouseButton = dragState.MouseButton;
            ModifierKeys = dragState.ModifierKeys;
        }
    }

    public class MouseClickEventArgs : EventArgs
    {
        public Vector2 Position { get; private set; }
        public MouseButtons MouseButton { get; private set; }
        public ModifierKeysState ModifierKeys { get; private set; }
        
        public MouseClickEventArgs(MouseClickState mouseClickState)
        {
            Position = mouseClickState.Position;
            MouseButton = mouseClickState.MouseButton;
            ModifierKeys = mouseClickState.ModifierKeys;
        }
    }

    public class MouseClickState
    {
        public Vector2 Position { get; set; }
        public MouseButtons MouseButton { get; set; }
        public ModifierKeysState ModifierKeys { get; set; }

        public MouseClickState()
        {
        }

        public MouseClickState(Vector2 position, MouseButtons mouseButton, ModifierKeysState modifierKeys)
        {
            Position = position;
            MouseButton = mouseButton;
            ModifierKeys = modifierKeys;
        }
    }

    public class MouseDragState
    {
        public bool IsDragging { get; set; }
        public MouseButtons MouseButton { get; set; }
        public Vector2 StartPosition { get; set; }
        public Vector2 EndPosition { get; set; }
        public ModifierKeysState ModifierKeys { get; set; }

        public MouseDragState()
        {
            
        }

        public MouseDragState(bool isDragging, MouseButtons mouseButton, Vector2 startPosition, Vector2 endPosition, ModifierKeysState modifierKeys)
        {
            IsDragging = isDragging;
            MouseButton = mouseButton;
            StartPosition = startPosition;
            EndPosition = endPosition;
            ModifierKeys = modifierKeys;
        }
    }

    public class ModifierKeysState
    {
        public bool IsShiftKeyDown { get; set; }
        public bool IsControlKeyDown { get; set; }
        public bool IsAltKeyDown { get; set; }

        public ModifierKeysState()
        {
            
        }

        public ModifierKeysState(KeyboardState keyboardState)
        {
            IsShiftKeyDown = keyboardState.IsKeyDown(Keys.LeftShift) || keyboardState.IsKeyDown(Keys.RightShift);
            IsControlKeyDown = keyboardState.IsKeyDown(Keys.LeftControl) || keyboardState.IsKeyDown(Keys.RightControl);
            IsAltKeyDown = keyboardState.IsKeyDown(Keys.LeftAlt) || keyboardState.IsKeyDown(Keys.RightAlt);
        }
    }

    public class InputManager
    {
        public KeyboardState CurrentKeyboardState { get; private set; }
        public KeyboardState PreviousKeyboardState { get; private set; }
        public MouseState CurrentMouseState { get; private set; }
        public MouseState PreviousMouseState { get; private set; }

        public Vector2 MousePosition
        {
            get { return CurrentMouseState.GetPosition(); }
        }

        public MouseButtons LastReleasedMouseButton
        {
            get
            {
                if (WasMouseReleased(MouseButtons.Left))
                {
                    return MouseButtons.Left;
                }

                if (WasMouseReleased(MouseButtons.Middle))
                {
                    return MouseButtons.Middle;
                }

                if (WasMouseReleased(MouseButtons.Right))
                {
                    return MouseButtons.Right;
                }

                return MouseButtons.None;
            }
        }

        public MouseButtons LastPressedMouseButton
        {
            get
            {
                if (WasMousePressed(MouseButtons.Left))
                {
                    return MouseButtons.Left;
                }

                if (WasMousePressed(MouseButtons.Middle))
                {
                    return MouseButtons.Middle;
                }

                if (WasMousePressed(MouseButtons.Right))
                {
                    return MouseButtons.Right;
                }

                return MouseButtons.None;
            }
        }

        private MouseDragState _mouseDragState = new MouseDragState();

        public const float MouseDragThreshold = 5.0f;

        public event EventHandler<MouseDragEventArgs> MouseDragCompleted;
        public event EventHandler<MouseDragEventArgs> MouseDragBegan;
        public event EventHandler<MouseClickEventArgs> MouseClicked;

        private double _inputUpdateElapsed;
        private static readonly double InputUpdateRate = TimeSpan.FromSeconds(1.0 / 60).TotalMilliseconds;

        private void OnMouseClicked(MouseClickState clickState)
        {
            var handler = MouseClicked;
            if (handler != null)
            {
                handler(this, new MouseClickEventArgs(clickState));
            }
        }

        private void OnMouseDragBegan(MouseDragState dragState)
        {
            var handler = MouseDragBegan;
            if (handler != null)
            {
                handler(this, new MouseDragEventArgs(dragState));
            }
        }

        private void OnMouseDragCompleted(MouseDragState dragState)
        {
            var handler = MouseDragCompleted;
            if (handler != null)
            {
                handler(this, new MouseDragEventArgs(dragState));
            }
        }

        public void Update()
        {
            PreviousKeyboardState = CurrentKeyboardState;
            PreviousMouseState = CurrentMouseState;

            CurrentKeyboardState = Keyboard.GetState();
            CurrentMouseState = Mouse.GetState();

            UpdateMouseState();
        }
        
        private void UpdateMouseState()
        {
            if (WasMousePressed(MouseButtons.Any))
            {
                _mouseDragState = new MouseDragState()
                {
                    IsDragging = false,
                    StartPosition = MousePosition,
                    EndPosition = MousePosition,
                    MouseButton = LastPressedMouseButton,
                    ModifierKeys = CurrentKeyboardState.GetModifierKeysState()
                };
            }
            else if (WasMouseReleased(MouseButtons.Any))
            {
                if (!_mouseDragState.IsDragging)
                {
                    var clickState = new MouseClickState()
                    {
                        Position = MousePosition,
                        MouseButton = _mouseDragState.MouseButton,
                        ModifierKeys = _mouseDragState.ModifierKeys
                    };

                    OnMouseClicked(clickState);
                }
                else
                {
                    OnMouseDragCompleted(_mouseDragState);
                    _mouseDragState.IsDragging = false;

                    return;
                }
            }

            if (IsMouseDown(_mouseDragState.MouseButton))
            {
                _mouseDragState.EndPosition = MousePosition;

                if (!_mouseDragState.IsDragging)
                {
                    if (Vector2.Distance(_mouseDragState.EndPosition, _mouseDragState.StartPosition) > MouseDragThreshold)
                    {
                        _mouseDragState.IsDragging = true;
                        OnMouseDragBegan(_mouseDragState);
                    }
                }
            }
        }

        public bool ShouldHandleRepeatingInput(GameTime gameTime)
        {
            _inputUpdateElapsed += gameTime.ElapsedGameTime.TotalMilliseconds;

            if (_inputUpdateElapsed >= InputUpdateRate)
            {
                _inputUpdateElapsed -= InputUpdateRate;
                return true;
            }

            return false;
        }

        public bool WasKeyPressed(Keys key)
        {
            return CurrentKeyboardState.IsKeyDown(key) && PreviousKeyboardState.IsKeyUp(key);
        }

        public bool WasKeyReleased(Keys key)
        {
            return PreviousKeyboardState.IsKeyDown(key) && CurrentKeyboardState.IsKeyUp(key);
        }

        public bool IsShiftKeyDown()
        {
            return IsKeyDown(Keys.LeftShift) || IsKeyDown(Keys.RightShift);
        }

        public bool IsControlKeyDown()
        {
            return IsKeyDown(Keys.LeftControl) || IsKeyDown(Keys.RightControl);
        }

        public bool IsAltKeyDown()
        {
            return IsKeyDown(Keys.LeftAlt) || IsKeyDown(Keys.RightAlt);
        }

        public bool IsKeyDown(Keys key)
        {
            return CurrentKeyboardState.IsKeyDown(key);
        }

        public bool IsKeyUp(Keys key)
        {
            return CurrentKeyboardState.IsKeyUp(key);
        }

        private bool WasMouseClicked(MouseButtons button)
        {
            return WasMouseReleased(button) && !_mouseDragState.IsDragging;
        }

        public bool WasMousePressed(MouseButtons button)
        {
            if (!ParticleTestGame.ClientBounds.Contains(MousePosition.ToPoint()))
            {
                return false;
            }

            if (button == MouseButtons.Left)
            {
                return CurrentMouseState.LeftButton == ButtonState.Pressed &&
                       PreviousMouseState.LeftButton == ButtonState.Released;
            }

            if (button == MouseButtons.Middle)
            {
                return CurrentMouseState.MiddleButton == ButtonState.Pressed &&
                       PreviousMouseState.MiddleButton == ButtonState.Released;
            }

            if (button == MouseButtons.Right)
            {
                return CurrentMouseState.RightButton == ButtonState.Pressed &&
                       PreviousMouseState.RightButton == ButtonState.Released;
            }

            if (button == MouseButtons.Any)
            {
                return (CurrentMouseState.LeftButton == ButtonState.Pressed &&
                        PreviousMouseState.LeftButton == ButtonState.Released) ||
                       (CurrentMouseState.MiddleButton == ButtonState.Pressed &&
                        PreviousMouseState.MiddleButton == ButtonState.Released) ||
                       (CurrentMouseState.RightButton == ButtonState.Pressed &&
                        PreviousMouseState.RightButton == ButtonState.Released);
            }

            return false;
        }

        public bool WasMouseReleased(MouseButtons button)
        {
            if (button == MouseButtons.Left)
            {
                return PreviousMouseState.LeftButton == ButtonState.Released &&
                       CurrentMouseState.LeftButton == ButtonState.Pressed;
            }

            if (button == MouseButtons.Middle)
            {
                return PreviousMouseState.MiddleButton == ButtonState.Released &&
                       CurrentMouseState.MiddleButton == ButtonState.Pressed;
            }

            if (button == MouseButtons.Right)
            {
                return PreviousMouseState.RightButton == ButtonState.Released &&
                       CurrentMouseState.RightButton == ButtonState.Pressed;
            }

            if (button == MouseButtons.Any)
            {
                return (CurrentMouseState.LeftButton == ButtonState.Released &&
                        PreviousMouseState.LeftButton == ButtonState.Pressed) ||
                       (CurrentMouseState.MiddleButton == ButtonState.Released &&
                        PreviousMouseState.MiddleButton == ButtonState.Pressed) ||
                       (CurrentMouseState.RightButton == ButtonState.Released &&
                        PreviousMouseState.RightButton == ButtonState.Pressed);
            }

            return false;
        }

        public bool IsMouseDown(MouseButtons button)
        {
            if (button == MouseButtons.Left)
            {
                return CurrentMouseState.LeftButton == ButtonState.Pressed;
            }

            if (button == MouseButtons.Middle)
            {
                return PreviousMouseState.MiddleButton == ButtonState.Pressed;
            }

            if (button == MouseButtons.Right)
            {
                return PreviousMouseState.RightButton == ButtonState.Pressed;
            }

            if (button == MouseButtons.Any)
            {
                return CurrentMouseState.LeftButton == ButtonState.Pressed ||
                       CurrentMouseState.MiddleButton == ButtonState.Pressed ||
                       CurrentMouseState.RightButton == ButtonState.Pressed;
            }

            return false;
        }

        public bool IsMouseUp(MouseButtons button)
        {
            if (button == MouseButtons.Left)
            {
                return CurrentMouseState.LeftButton == ButtonState.Released;
            }

            if (button == MouseButtons.Middle)
            {
                return PreviousMouseState.MiddleButton == ButtonState.Released;
            }

            if (button == MouseButtons.Right)
            {
                return PreviousMouseState.RightButton == ButtonState.Released;
            }

            if (button == MouseButtons.Any)
            {
                return CurrentMouseState.LeftButton == ButtonState.Released ||
                       CurrentMouseState.MiddleButton == ButtonState.Released ||
                       CurrentMouseState.RightButton == ButtonState.Released;
            }

            return false;
        }

        public MouseButtons GetReleasedMouseButton()
        {
            if (WasMouseReleased(MouseButtons.Left))
            {
                return MouseButtons.Left;
            }

            if (WasMouseReleased(MouseButtons.Middle))
            {
                return MouseButtons.Middle;
            }

            if (WasMouseReleased(MouseButtons.Right))
            {
                return MouseButtons.Right;
            }

            return MouseButtons.None;
        }
    }

    public enum MouseButtons
    {
        None,
        Left,
        Middle,
        Right,
        Any
    }
}
