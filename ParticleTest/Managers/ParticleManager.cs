﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using ParticleTest.Effects;
using ParticleTest.Entities;
using ParticleTest.Util;

namespace ParticleTest.Managers
{
    public class ParticleManager : DrawableGameComponent
    {
        public static float BounceReflectionModifier { get; set; }
        public const float GravitationalForce = 180.0f;
        public static Vector2 Gravity = new Vector2(0, GravitationalForce);
        public static bool GravityEnabled { get; set; }
        public static bool WindowBoundriesEnabled { get; set; }
        public static bool ParticleFadingEnabled { get; set; }
        public static bool SubframeInterpolationEnabled { get; set; }
        public static bool BloomEnabled { get; set; }
        public static BloomSettings.Presets CurrentBloomSettingsPreset { get; set; }

        public static readonly Color RedParticleColor = new Color(255, 50, 50);
        public static readonly Color BlueParticleColor = new Color(40, 75, 255);
        public static readonly Color GreenParticleColor = new Color(125, 255, 100);
        public static readonly Color YellowParticleColor = new Color(255, 255, 100);

        private const int MaxParticleCount = 100000;
        private const float LauncherHeightOffset = 10f;

        public readonly ParticlePool Particles = new ParticlePool(MaxParticleCount);
        public readonly List<IEntity> Entities = new List<IEntity>();
        public readonly List<IModifier> Modifiers = new List<IModifier>();

        private ISelectable _selectedEntity;
        public ISelectable SelectedEntity
        {
            get { return _selectedEntity; }
            set
            {
                if (_selectedEntity != null)
                    _selectedEntity.IsSelected = false;

                if (value != null)
                    value.IsSelected = true;

                _selectedEntity = value;
            }
        }

        public bool IsAdjustingSelectedEntity
        {
            get
            {
                var entity = SelectedEntity as IAdjustable;
                return entity != null && (entity.IsBeingMoved || entity.IsAdjusting);
            }
        }

        public bool IsCreatingLauncher { get; set; }
        public bool IsCreatingEmitter { get; set; }
        
        private Vector2 _dragStart;
        private Vector2 _dragEnd;
        
        private static readonly TimeSpan LauncherFireRate = TimeSpan.FromSeconds(5);

        private SpriteBatch _spriteBatch;
        private PrimitiveBatch _primitiveBatch;

        public ParticleManager(Game game)
            : base(game)
        {
            Visible = false;

            WindowBoundriesEnabled = false;
            BounceReflectionModifier = -0.8f;
            GravityEnabled = true;
            ParticleFadingEnabled = true;
            SubframeInterpolationEnabled = true;
            BloomEnabled = true;
        }

        public override void Initialize()
        {
            ParticleTestGame.InputManager.MouseDragCompleted += OnMouseDragCompleted;
            ParticleTestGame.InputManager.MouseDragBegan += OnMouseDragBegan;
            ParticleTestGame.InputManager.MouseClicked += OnMouseClicked;

            base.Initialize();
        }

        protected override void LoadContent()
        {
            _spriteBatch = new SpriteBatch(GraphicsDevice);
            _primitiveBatch = new PrimitiveBatch(GraphicsDevice);
        }

        public void HandleInput(InputManager inputManager)
        {
            if (SelectedEntity != null)
            {
                var adjustableEntity = SelectedEntity as IAdjustable;
                adjustableEntity?.HandleInputForAdjustment(inputManager);
            }
            else
            {
                if (IsCreatingEmitter || IsCreatingLauncher)
                    _dragEnd = inputManager.MousePosition;
            }
        }

        public override void Update(GameTime gameTime)
        {
            foreach (var entity in Entities)
            {
                entity.Update(gameTime);   
            }

            Entities.RemoveAll(o => !o.IsActive);

            for (var i = 0; i < Particles.Count; i++)
            {
                Particles.Update(i, gameTime);
            }

            Particles.Reorder();
        }

        public override void Draw(GameTime gameTime)
        {
            _spriteBatch.Begin(SpriteSortMode.FrontToBack, BlendState.AlphaBlend);

            for (var i = 0; i < Particles.Count; i++)
            {
                Particles[i].Draw(_spriteBatch);
            }

            _spriteBatch.End();

            //_primitiveBatch.Begin(PrimitiveType.TriangleList);

            //for (var i = 0; i < Particles.Count; i++)
            //{
            //    _primitiveBatch.AddVertex(Particles[i].Position, Particles[i].Color);
            //    _primitiveBatch.AddVertex(Particles[i].Position + Vector2.UnitX, Particles[i].Color);
            //    _primitiveBatch.AddVertex(Particles[i].Position + Vector2.UnitY, Particles[i].Color);
            //}

            //_primitiveBatch.End();

            _spriteBatch.Begin();

            foreach (var entity in Entities)
            {
                entity.Draw(_spriteBatch);
            }

            if (IsCreatingEmitter)
            {
                _spriteBatch.DrawLine(_dragStart, _dragEnd, TargetIndicatorProperties.LineThickness, TargetIndicatorProperties.LineColor);

                _spriteBatch.Draw(
                    texture: ResourceManager.CircleTexture.Texture,
                    position: _dragStart,
                    sourceRectangle: null,
                    color: TargetIndicatorProperties.HandleColor,
                    rotation: 0,
                    origin: ResourceManager.CircleTexture.Origin,
                    scale: TargetIndicatorProperties.HandleScale,
                    effects: SpriteEffects.None,
                    layerDepth: 0
                );
            }
            else if (IsCreatingLauncher)
            {
                var currentDragPosition = new Vector2(_dragStart.X, ParticleTestGame.ClientBounds.Height - LauncherHeightOffset);
                _spriteBatch.DrawLine(currentDragPosition, _dragEnd, TargetIndicatorProperties.LineThickness, TargetIndicatorProperties.LineColor);

                _spriteBatch.Draw(
                    texture: ResourceManager.CircleTexture.Texture,
                    position: currentDragPosition,
                    sourceRectangle: null,
                    color: TargetIndicatorProperties.HandleColor,
                    rotation: 0,
                    origin: ResourceManager.CircleTexture.Origin,
                    scale: TargetIndicatorProperties.HandleScale,
                    effects: SpriteEffects.None,
                    layerDepth: 0
                );
            }

            _spriteBatch.End();
        }

        public void Reset()
        {
            SelectedEntity = null;
            Modifiers.Clear();
            Entities.Clear();
            Particles.Clear();
        }

        public void AddParticle(Particle particle)
        {
            Particles.Add(particle);
        }
        
        public void AddEntity(IEntity entity)
        {
            Entities.Add(entity);

            var forcefield = entity as Forcefield;
            if (forcefield != null)
            {
                Modifiers.Add(forcefield);
            }
        }
        
        public void RemoveEntity(IEntity entity)
        {
            if (entity == null)
                return;

            if (SelectedEntity == entity)
                SelectedEntity = null;

            Entities.Remove(entity);

            var modifier = entity as IModifier;
            if (modifier != null)
                Modifiers.Remove(modifier);
        }
        
        public bool TryBeginDragging(Vector2 position)
        {
            var clickedAdjustableEntity = GetEntityAtPosition(position) as IAdjustable;

            if (clickedAdjustableEntity == null)
            {
                // Didn't start dragging an entity

                var selectedAdjustableEntity = SelectedEntity as IAdjustable;

                if (selectedAdjustableEntity == null)
                    return false;
                
                // If an an adjustable entity is selected, check if its adjustment handle was dragged
                if (Vector2.Distance(selectedAdjustableEntity.Target, position) > InputManager.MouseDragThreshold)
                    return false;

                selectedAdjustableEntity.IsAdjusting = true;
                return true;
            }
            else
            {
                // Started dragging an entity
                if (Vector2.Distance(clickedAdjustableEntity.Position, position) > InputManager.MouseDragThreshold)
                    return false;

                SelectedEntity = clickedAdjustableEntity;
                clickedAdjustableEntity.IsBeingMoved = true;

                return true;
            }
        }

        public IEntity GetEntityAtPosition(Vector2 position)
        {
            return Entities.FirstOrDefault(entity => (entity.Position - position).Length() <= 10);
        }

        private void OnMouseClicked(object sender, MouseClickEventArgs args)
        {
            var clickedEntity = GetEntityAtPosition(args.Position);

            if (args.MouseButton == MouseButtons.Left)
            {
                if (clickedEntity == null && SelectedEntity == null)
                {
                    AddEntity(new Forcefield(args.Position, args.ModifierKeys.IsShiftKeyDown ? -100 : 100));
                }
                else
                {
                    SelectedEntity = clickedEntity as ISelectable;
                }
            }
            else if (args.MouseButton == MouseButtons.Right)
            {
                RemoveEntity(clickedEntity);
            }
        }

        private void OnMouseDragBegan(object sender, MouseDragEventArgs args)
        {
            if (!TryBeginDragging(args.StartPosition))
            {
                SelectedEntity = null;

                if (args.MouseButton == MouseButtons.Right)
                {
                    IsCreatingLauncher = true;
                }
                else
                {
                    IsCreatingEmitter = true;
                }

                _dragStart = args.StartPosition;
            }
        }

        private void OnMouseDragCompleted(object sender, MouseDragEventArgs args)
        {
            if (IsAdjustingSelectedEntity)
            {
                var entity = SelectedEntity as IAdjustable;
                if (entity == null)
                {
                    return;
                }
                
                entity.IsAdjusting = false;
                entity.IsBeingMoved = false;
            }
            else if (IsCreatingEmitter || IsCreatingLauncher)
            {
                if (IsCreatingEmitter)
                {
                    IsCreatingEmitter = false;

                    var emitter = new Emitter(args.StartPosition, args.EndPosition - args.StartPosition, 500);
                    AddEntity(emitter);
                }
                else if (IsCreatingLauncher)
                {
                    IsCreatingLauncher = false;

                    var position = new Vector2(args.StartPosition.X, ParticleTestGame.ClientBounds.Height - LauncherHeightOffset);
                    var launcher = new Launcher(position, args.EndPosition - position, LauncherFireRate);
                    AddEntity(launcher);
                }
            }
        }
    }
}
