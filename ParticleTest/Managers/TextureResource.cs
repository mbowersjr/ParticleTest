﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using ParticleTest.Util;

namespace ParticleTest.Managers
{
    public class TextureResource : IDisposable
    {
        public Texture2D Texture { get; private set; }
        public Vector2 Origin { get; private set; }

        public TextureResource(Texture2D texture, Vector2 origin)
        {
            Texture = texture;
            Origin = origin;
        }

        public TextureResource(Texture2D texture, Point origin)
            : this(texture, origin.ToVector2())
        {   
        }

        public TextureResource(Texture2D texture)
            : this(texture, texture.Bounds.Center)
        {
        }
        
        public TextureResource()
        {
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private bool _disposed;
        protected virtual void Dispose(bool disposing)
        {
            if (_disposed || !disposing)
                return;
            
            if (Texture != null && !Texture.IsDisposed)
            {
                Texture.Dispose();
            }

            _disposed = true;
        }
    }
}