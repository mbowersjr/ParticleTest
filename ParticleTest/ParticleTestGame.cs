using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using ParticleTest.Effects;
using ParticleTest.Managers;
using ParticleTest.Util;

namespace ParticleTest
{
    public class ParticleTestGame : Game
    {
        private GraphicsDeviceManager _graphics;
        private SpriteBatch _spriteBatch;
        
        public static BloomComponent BloomEffect { get; private set; }
        public static Rectangle ClientBounds { get; private set; }
        public static FrameRateCounter FrameRateCounter { get; private set; }
        public static InputManager InputManager { get; private set; }
        public static InterfaceManager InterfaceManager { get; private set; }
        public static ParticleManager ParticleManager { get; private set; }

        public ParticleTestGame()
        {
            _graphics = new GraphicsDeviceManager(this)
            {
                SynchronizeWithVerticalRetrace = false,
                PreferredBackBufferWidth = 1024,
                PreferredBackBufferHeight = 768,
                IsFullScreen = false
            };
            _graphics.ApplyChanges();
            
            ClientBounds = GraphicsDevice.Viewport.TitleSafeArea;
            
            IsFixedTimeStep = false;
            IsMouseVisible = true;
            Window.AllowUserResizing = true;
            Window.ClientSizeChanged += (sender, args) => { ClientBounds = GraphicsDevice.Viewport.TitleSafeArea; };

            Content.RootDirectory = "Content";

            InputManager = new InputManager();
            FrameRateCounter = new FrameRateCounter(this);
            ParticleManager = new ParticleManager(this);
            InterfaceManager = new InterfaceManager(this);

            Components.Add(ParticleManager);
            Components.Add(InterfaceManager);
            Components.Add(FrameRateCounter);

            BloomEffect = new BloomComponent(this);
            Components.Add(BloomEffect);
            BloomEffect.Settings = BloomSettings.GetPreset(ParticleManager.CurrentBloomSettingsPreset);
        }

        //protected override void Initialize()
        //{

        //    base.Initialize();
        //}

        protected override void LoadContent()
        {
            _spriteBatch = new SpriteBatch(GraphicsDevice);

            ResourceManager.Load(this);
        }

        protected override void UnloadContent()
        {
            ResourceManager.Unload();
        }

        protected override void Update(GameTime gameTime)
        {
            InputManager.Update();
            InterfaceManager.Update(gameTime);

            HandleInput();
            
            if (ParticleManager.SelectedEntity != null)
                ParticleManager.SelectedEntity.HandleInput(InputManager, gameTime);

            ParticleManager.HandleInput(InputManager);
            
            base.Update(gameTime);
        }

        private static readonly int _bloomPresetsCount = Enum.GetValues(typeof(BloomSettings.Presets)).Length;

        private void HandleInput()
        {
            if (InputManager.WasKeyPressed(Keys.Escape) || InputManager.WasKeyPressed(Keys.Q)) 
            {
                Exit();
            }

            var updateInstructions = false;

            if (InputManager.WasKeyPressed(Keys.R))
            {
                ParticleManager.Reset();
            }
            
            if (InputManager.WasKeyPressed(Keys.P))
            {
                if (InputManager.IsShiftKeyDown())
                {
                    ParticleManager.BloomEnabled = !ParticleManager.BloomEnabled;
                }
                else
                {
                    var currentIndex = (int)ParticleManager.CurrentBloomSettingsPreset;
                    ParticleManager.CurrentBloomSettingsPreset = (BloomSettings.Presets)(currentIndex == _bloomPresetsCount - 1 ? 0 : currentIndex + 1);
                    BloomEffect.Settings = BloomSettings.GetPreset(ParticleManager.CurrentBloomSettingsPreset);
                }

                updateInstructions = true;
            }

            if (InputManager.WasKeyPressed(Keys.S))
            {
                ParticleManager.SubframeInterpolationEnabled = !ParticleManager.SubframeInterpolationEnabled;
                updateInstructions = true;
            }

            if (InputManager.WasKeyPressed(Keys.W))
            {
                ParticleManager.WindowBoundriesEnabled = !ParticleManager.WindowBoundriesEnabled;
                updateInstructions = true;
            }

            if (InputManager.WasKeyPressed(Keys.F))
            {
                ParticleManager.ParticleFadingEnabled = !ParticleManager.ParticleFadingEnabled;
                updateInstructions = true;
            }

            if (InputManager.WasKeyPressed(Keys.D))
            {
                InterfaceManager.ShowDebugDisplay = !InterfaceManager.ShowDebugDisplay;
                updateInstructions = true;
            }

            if (InputManager.WasKeyPressed(Keys.G))
            {
                ParticleManager.GravityEnabled = !ParticleManager.GravityEnabled;
                updateInstructions = true;
            }

            if (updateInstructions)
            {
                InterfaceManager.UpdateInstructions();
            }
        }

        protected override void Draw(GameTime gameTime)
        {
            if (ParticleManager.BloomEnabled)
                BloomEffect.BeginDraw();

            GraphicsDevice.Clear(InterfaceManager.BackgroundColor);

            ParticleManager.Draw(gameTime);

            base.Draw(gameTime);

            InterfaceManager.Draw(gameTime);
        }
    }
}
